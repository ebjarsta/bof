let switch1 = 0;
let switch2 = 0;
let switch3 = 0;
let switch4 = 0;
let Power = 0;

document.getElementById('returnButton').addEventListener('click', function() {
  location.replace("../../HTML/index.html?0")
});


function updateCode() {

  //document.getElementById("codeResult").innerText = code;
  const code = "" + switch1 + switch2 + switch3 + switch4;
  window.code = code;
  if (code === "0110") {
    document.getElementById("codeResult").classList.add("code-found");
    document.body.style.backgroundImage = "url('../../Images/disjoncteur/disjoncteur.png')";
    disableSwitches();
    Power=1;
    location.replace("../../HTML/index.html?1");
  } else {
    document.getElementById("codeResult").classList.remove("code-found");
    enableSwitches();
  }
}

function disableSwitches() {
  document.getElementById("switch1").disabled = true;
  document.getElementById("switch2").disabled = true;
  document.getElementById("switch3").disabled = true;
  document.getElementById("switch4").disabled = true;
}

function enableSwitches() {
  document.getElementById("switch1").disabled = false;
  document.getElementById("switch2").disabled = false;
  document.getElementById("switch3").disabled = false;
  document.getElementById("switch4").disabled = false;
}

document.getElementById("switch1").addEventListener("change", function() {
  switch1 = this.checked ? 1 : 0;
  const led1 = document.getElementById("led1");
  led1.style.backgroundColor = switch1 ? 'yellow' : '#69646b';
  led1.style.filter= switch1 ? 'blur(3px)' : 'blur(0px)';
  updateCode();
});

document.getElementById("switch2").addEventListener("change", function() {
  switch2 = this.checked ? 1 : 0;
  const led2 = document.getElementById("led2");
  led2.style.backgroundColor = switch2 ? 'yellow' : '#69646b';
  led2.style.filter= switch2 ? 'blur(3px)' : 'blur(0px)';
  updateCode();
});

document.getElementById("switch3").addEventListener("change", function() {
  switch3 = this.checked ? 1 : 0;
  const led3 = document.getElementById("led3");
  led3.style.backgroundColor = switch3 ? 'yellow' : '#69646b';
  led3.style.filter= switch3 ? 'blur(3px)' : 'blur(0px)';
  updateCode();
});

document.getElementById("switch4").addEventListener("change", function() {
  switch4 = this.checked ? 1 : 0;
  const led4 = document.getElementById("led4");
  led4.style.backgroundColor = switch4 ? 'yellow' : '#69646b';
  led4.style.filter= switch4 ? 'blur(3px)' : 'blur(0px)';
  updateCode();
});
