let interrupteur = false;
let torche = false;
let electricite = 0;
let isModalOpen = false;

//Pour pouvoir changer le volume de la musique de fond
const backgroundAudio = document.getElementById("backgroundAudio");
if (backgroundAudio) {
    // Set the volume to 0.5 (half of the maximum volume)
    backgroundAudio.volume = 0.3;
  } else {
    console.error("Audio element not found");
  }

//Pour jouer un son quand un objet est trouve
function playClickSound() {
    const clickSound = document.getElementById("clickSoundTrouve");
    if (clickSound) {
      clickSound.volume = 0.2;
      clickSound.play();
    } else {
      console.error("Audio element not found");
    }
  }

// Flag to ensure audio is played only once
var audioPlayed = false;

function playAudio() {
    if (!audioPlayed) {
        var backgroundAudio = document.getElementById('backgroundAudio');
        var firealarm = document.getElementById('firealarm');
        backgroundAudio.play();
        firealarm.play();
        backgroundAudio.volume = 0.2;
        firealarm.volume = 0.2;
        audioPlayed = true; // Set the flag so audio is not played again
    }
}

// Add event listener for the first click
document.addEventListener('click', playAudio);

window.addEventListener("DOMContentLoaded", () => {

    const initPosX = 4430;
    const initPosY = 6140;
    initBackground(initPosX, initPosY);

    const overlay = document.getElementById("overlay");
    const light = document.getElementById("light");
    const btnLight = document.getElementById("interrupteur");
    const background = document.getElementById("background");

    btnLight.addEventListener("click", () => {
        // if Power = 1 ? voir avec disjoncteur
        // Si on clique l'interrupteur de la lumière
        if (interrupteur && electricite === 1) { // soit l'interrupteur est déjà allumé, et on éteint la lumière
            overlay.style.display = "block";
            light.style.display = "block";
            interrupteur = false;
            background.style.backgroundImage = "url('../Images/Index/background_lightoff.jpg')";

        }
        else if (interrupteur === false && electricite === 1) { // soit l'interrupteur est éteint, et on allume la lumière
            overlay.style.display = "none";
            light.style.display = "none";
            interrupteur = true;
            background.style.backgroundImage = "url('../Images/Index/background.jpg')";

        }
    });

    // Montre
    let statut_montre = false;
    const montre = document.getElementById("watch");
    const temps = document.getElementById("chrono");
    const btnMontre = document.getElementById("slot4"); // case de l'inventaire contenant la montre

    // btnMontre.click();
    // Démarre le timer mais on met un délai pour que commence un tt petit peu après
    setTimeout(() => {
        countdown("chrono", 10, 0);
    }, 100);

    btnMontre.addEventListener("click", () => {
        // console.log(statut_montre, demarrage)

        if (!statut_montre) {
            montre.style.display = "block";
            temps.style.display = "block";
            statut_montre = true;
        }
        else {
            montre.style.display = "none";
            temps.style.display = "none";
            statut_montre = false;
        }

        // console.log(statut_montre, demarrage);
    });

    // Lampe torche
    const torch = document.getElementById("torch");
    let slot = document.getElementById('slot2');
    slot.classList.add("complete");
    slot.innerHTML = '<img id="icone_lampe_torche_inventaire" src="../Images/Index/lampe_torche.png" alt="lampe torche" style="max-width: 100%; max-height: 100%;">';

    slot.addEventListener("click", () => {
        // Si on clique sur la torche pour voir dans le noir
        if(torche && !interrupteur){ // soit on est dans le noir et la torche est allumée, et on éteint la torche
            torche = false;
            torch.style.display = "none";
            overlay.style.display = "block";
        }
        else if(!torche){ // soit la torche est éteinte, et on allume la torche
            torche = true;
            torch.style.display = "block";
        }
        else { // soit la torche est allumée mais l'interrupteur est aussi allumé, et on éteint simplement la torche
            torche = false;
            torch.style.display = "none";
        }
    });
    let container = document.getElementById('container');

    document.addEventListener("mousemove", (ev) => {
        if (torche) {
            let containerRect = container.getBoundingClientRect();

            // Calculate local coordinates within the container
            let x = ev.pageX - containerRect.left;
            let y = ev.pageY - containerRect.top;

            const gradientSize = 200; // Taille du gradient radial (ajustez selon vos besoins)

            overlay.style.display = "none";
            light.style.background = `radial-gradient(circle at ${x}px ${y}px, transparent, rgba(0, 0, 0, 0.98) ${gradientSize}px)`;
        }
    });




// Déplacement 360
    document.body.addEventListener("keydown", (ev) => {
        const bgPosY = parseInt(document.getElementById("background").style.backgroundPositionY);

        if(ev.code === "ArrowLeft"){ // bgPosX < 6270
            moveBackground(+100, 0);
            moveObjects(+100, 0);
        }
        else if(ev.code === "ArrowRight"){ // bgPosX > 5470
            moveBackground(-100, 0);
            moveObjects(-100, 0);
        }
        else if(ev.code === "ArrowUp" && bgPosY < 6400){ // false
            moveBackground(0, +50);
            moveObjects(0, +50);
        }
        else if(ev.code === "ArrowDown" && bgPosY > 5840){ // false
            moveBackground(0, -50);
            moveObjects(0, -50);
        }
    });

});


function initBackground(x, y){
    // Initialise la position de l'image de fond 360°

    document.getElementById("background").style.backgroundPositionX = x.toString() + "px";
    document.getElementById("background").style.backgroundPositionY = y.toString() + "px";

    return true
}


function moveBackground(x, y){
    // Déplace l'image de fond 360°

    const bgPosX = parseInt(document.getElementById("background").style.backgroundPositionX);
    const bgPosY = parseInt(document.getElementById("background").style.backgroundPositionY);

    document.getElementById("background").style.backgroundPositionX = (bgPosX + x).toString() + "px";
    document.getElementById("background").style.backgroundPositionY = (bgPosY + y).toString() + "px";

    return true
}


function moveObjects(x, y) {
    // Déplace l'ensemble des objets placés sur l'image de fond à 360°

    let objects = document.getElementsByClassName("object");
    const objPos = Array.from(objects).map((object) => [object.id, parseInt(object.style.top), parseInt(object.style.left)]);

    for (let i = 0; i < objPos.length; i++) {
        if (objPos[i][2] + x < 0) {
            document.getElementById(objPos[i][0]).style.left = (objPos[i][2] + x + 2560).toString() + "px";
        } else {
            document.getElementById(objPos[i][0]).style.left = ((objPos[i][2] + x) % 2560).toString() + "px";
        }

        document.getElementById(objPos[i][0]).style.top = (objPos[i][1] + y).toString() + "px";
    }

    return true;
}

function afficher_pc() {
    if (!isModalOpen) {
        if (electricite === 1) {
            document.getElementById('pcpopupContainer').style.display = 'block';
        } else {
            $('#modalTitle').text('Objet trouvé');
            $('#modalBody').html('On dirait un PC, il est branché mais il n\'y a pas de courant...');
            $('#modal').modal('show');
        }
    }
}

function found(mes){
    // Action à réaliser lors du clic sur l'un des objets placés sur l'image de fond à 360°
    if (!isModalOpen) {
            isModalOpen = true; // pour pas que y ait plusieurs pop up en même temps

            if (mes === "pc_prof") {
                if (interrupteur === true) {
                    // afficher le pc
                    document.getElementById('pcpopupContainer').style.display = 'block';
                    // faudra mettre isModalOpen à false là où tu fermes le popup
                    isModalOpen = false;
                } else {
                    isModalOpen = false;
                    noElec();
                }
            }
            else if (mes === "disjoncteur") {
                document.getElementById('disjpopupContainer').style.display = 'flex';
                isModalOpen = false;
            }
            else if (mes === "projecteur") {
                if (interrupteur === true) {
                    const proj = document.getElementById('projmorse');
                    if (proj.style.animationName === 'morse-f12') {
                        proj.style.animation = 'none';
                        isModalOpen = false;
                    } else {
                        proj.style.animation = 'morse-f12 20s steps(1, end) infinite';
                        isModalOpen = false;
                    }

                } else {
                    noElec();
                    isModalOpen = false;
                }
            }
            return true
    }
}


function countdown(elementName, minutes, seconds) {
    // Calcul du décompte temporel du jeu

    let element, endTime, hours, mins, msLeft, time;

    function twoDigits(n) {
        return (n <= 9 ? "0" + n : n);
    }

    function updateTimer() {
        msLeft = endTime - (+new Date);

        if(msLeft < 1000){
            window.location.href = '../HTML/pageFin/perdu.html';
        } else {
            time = new Date(msLeft);
            hours = time.getUTCHours();
            mins = time.getUTCMinutes();
            element.innerHTML = (hours ? hours + ':' + twoDigits(mins) : twoDigits(mins)) + ':' + twoDigits(time.getUTCSeconds());
            setTimeout(updateTimer, time.getUTCMilliseconds() + 500);
        }
    }

    element = document.getElementById(elementName);
    endTime = (+new Date) + 1000 * (60 * minutes + seconds) + 500;
    updateTimer();
}

////////////////////////// Inventaire

function find(mes){
    let title = "Objet trouvé !";
    document.getElementById("TitreModal").innerHTML = title.toString();
    document.getElementById("modalBody").innerHTML = mes.toString();
    return true;
}

function collect(object){
    let slot;
    let message;
    if (object === 'code_morse') {
        message = "Félicitations ! Vous avez trouvé une feuille de code morse !";
        slot = document.getElementById('slot1'); // première case de l'inventaire
        slot.style.backgroundImage = 'none';
        slot.innerHTML = '<img id="icone_code_morse_inventaire" src="../Images/Index/code_morse.png" alt="code morse" style="max-width: 100%; max-height: 100%;">';
        slot.classList.add("complete");
        // Déclenche pop-up avec code morse
        slot.addEventListener('click', openMorse); // ouvre un pop-up quand on clique sur la case de la feuille de morse
        object = "";
        find(message); // pop-up objet trouvé avec le message custom
        // Pour qu'un objet ne soit pas cliquable quand il a déjà été collecté :
        // Récupère l'objet collecté
        const collectedObject = document.getElementById(object);

        // Supprime l'objet de la page
        collectedObject.parentNode.removeChild(collectedObject);
    } else if (object === 'carte_etu') {
        message = "Félicitations ! Vous avez retrouvé votre carte étudiante !";
        slot = document.getElementById('slot3');
        slot.classList.add("complete");
        slot.innerHTML = '<img id="icone_carte_etu_inventaire" src="../Images/Index/carte_etu.png" draggable="true" alt="carte etu" style="max-width: 100%; max-height: 100%;">';

        slot.addEventListener('dragstart', function (event) {
            event.dataTransfer.setData('text/plain', 'carte_etu');
        });
        object = "";
        find(message); // pop-up objet trouvé avec le message custom
        // Pour qu'un objet ne soit pas cliquable quand il a déjà été collecté :
        // Récupère l'objet collecté
        const collectedObject = document.getElementById(object);

        // Supprime l'objet de la page
        collectedObject.parentNode.removeChild(collectedObject);
    }

}



//////////////////// BADGEUSE //////////////////////

let porteOpen = false;

// Nouvelle fonction pour demander le code de redémarrage
function demanderCodeRedemarrage() {
    const codeAttendu = "0727";
    const codeUtilisateur = prompt("Courant rétabli. Entrez le code de redémarrage de la badgeuse à 4 chiffres:");
    return codeUtilisateur === codeAttendu;
}


// Modifiez la fonction dropOnBadgeuse
function dropOnBadgeuse(draggedObject) {
    // Vérifiez si l'objet dragué est la carte (objet 3)
    if (draggedObject === 'carte_etu') {
        // Demandez le code de redémarrage avant d'ouvrir la porte
        if (demanderCodeRedemarrage()) {
            document.getElementById('badgeuse_message').innerText = 'La porte est déverrouillée !';
            document.getElementById('badgeuse_image').src = '../Images/Index/badgeuse_verte.png';
            setTimeout(() => {
                window.location.href = '../HTML/pageFin/pageFin.html';
            }, 5000);
        } else {
            alert("Code de redémarrage incorrect.");
        }
    }
    else {
        document.getElementById('badgeuse_message').innerText = 'Lecture incorrecte.';
    }
}


// Ferme le pop-up avec comme id id_pop
function closePop(id_pop) {
    document.getElementById(id_pop).style.display = 'none';
    isModalOpen = false; // pour pas que y ait plusieurs pop up en même temps
}

function openBadgeuse() {
    // On vérifie si on a l'électricité et si on a les droits du PC (si non, mettre msg erreur ? )
    //if (interrupteur == true && droits_ordi == true){ ... }

    if (!isModalOpen && porteOpen === false) { // ouvre pas si la porte est déjà ouverte

        document.getElementById('badgeuse_image').src = '../Images/Index/badgeuse.png';
            
            
        const badgeuseModal = document.getElementById('pop_badgeuse');
        // ouvre le pop-up de la badgeuse
        badgeuseModal.style.display = 'block';
        // Tester si marche si on drop quand c'est pas en display du coup ?
        isModalOpen = true;


        if (electricite === 0){ // S'il n'y a pas d'électricité
            document.getElementById('badgeuse_message').innerText = 'Tu n\'as pas d\'électricité !';
        } else {

            //document.getElementById("contenu").innerHTML = '<p id="badgeuseMessage">Glissez et déposez la carte ici pour ouvrir la porte. Note : en cas d\'incendie, les portes se ferment automatiquement au bout d\'un certain temps pour limiter la propagation des flammes. Dans cette situation, seul le personnel autorisé peut déverouiller la porte.</p><img id="badgeuseImage" src="../Images/Index/badgeuse.png" alt="Badgeuse">';
            document.getElementById('badgeuse_message').innerText = 'Placez votre carte sur la badgeuse.';

            // Ajoute un événement de drop pour la badgeuse
            badgeuseModal.ondragover = function (event) {
                event.preventDefault();
            };

            badgeuseModal.ondrop = function (event) {
                event.preventDefault();
                const draggedObject = event.dataTransfer.getData("text");
                dropOnBadgeuse(draggedObject);
            };
        }
    }
}


function openMorse() {
    if (!isModalOpen){
        document.getElementById('pop_morse').style.display = 'block';
        isModalOpen = true;
    }
}

//Code Disjoncteur
document.addEventListener('DOMContentLoaded', function() {

    let switch1 = 0;
    let switch2 = 0;
    let switch3 = 0;
    let switch4 = 0;

    document.getElementById('closePopupDisjButton').addEventListener('click', function() {
        document.getElementById('disjpopupContainer').style.display = 'none';
        isModalOpen = false;
    });

    const overlay = document.getElementById("overlay");
    const light = document.getElementById("light");
    const background = document.getElementById("background");
    const proj = document.getElementById("projecteur");

    function updateCode() {
          const code = "" + switch1 + switch2 + switch3 + switch4;

          //document.getElementById("codeResult").innerText = code;

          if (code === "0110") {
              document.getElementById("codeResult").classList.add("code-found");
              const popupContent = document.querySelector('.disj_popup-content');
              popupContent.style.backgroundImage = "url('../Images/disjoncteur/disjoncteur.png')";
              disableSwitches();
              electricite = 1;
            //   proj.parentNode.removeChild(proj);
            proj.removeAttribute("data-toggle");
            proj.removeAttribute("data-target");


          } else {
              document.getElementById("codeResult").classList.remove("code-found");
              enableSwitches();
          }
      }
      function disableSwitches() {
        document.getElementById("switch1").disabled = true;
        document.getElementById("switch2").disabled = true;
        document.getElementById("switch3").disabled = true;
        document.getElementById("switch4").disabled = true;
      }

      function enableSwitches() {
        document.getElementById("switch1").disabled = false;
        document.getElementById("switch2").disabled = false;
        document.getElementById("switch3").disabled = false;
        document.getElementById("switch4").disabled = false;
      }

      document.getElementById("switch1").addEventListener("change", function() {
        playClickSoundDisj();
        switch1 = this.checked ? 1 : 0;
          const led1 = document.getElementById("led1");
          led1.style.backgroundColor = switch1 ? 'yellow' : '#69646b';
        led1.style.filter= switch1 ? 'blur(3px)' : 'blur(0px)';
        updateCode();
      });

      document.getElementById("switch2").addEventListener("change", function() {
        playClickSoundDisj();
        switch2 = this.checked ? 1 : 0;
          const led2 = document.getElementById("led2");
          led2.style.backgroundColor = switch2 ? 'yellow' : '#69646b';
        led2.style.filter= switch2 ? 'blur(3px)' : 'blur(0px)';
        updateCode();
      });

      document.getElementById("switch3").addEventListener("change", function() {
        playClickSoundDisj();
        switch3 = this.checked ? 1 : 0;
          const led3 = document.getElementById("led3");
          led3.style.backgroundColor = switch3 ? 'yellow' : '#69646b';
        led3.style.filter= switch3 ? 'blur(3px)' : 'blur(0px)';
        updateCode();
      });

      document.getElementById("switch4").addEventListener("change", function() {
        playClickSoundDisj();
        switch4 = this.checked ? 1 : 0;
          const led4 = document.getElementById("led4");
          led4.style.backgroundColor = switch4 ? 'yellow' : '#69646b';
        led4.style.filter= switch4 ? 'blur(3px)' : 'blur(0px)';
        updateCode();
      });

});

function playClickSoundDisj() {
    const clickSoundD = document.getElementById("clickSoundDisj");
    if (clickSoundD) {
      clickSoundD.volume = 0.2;
      clickSoundD.play();
    } else {
      console.error("Audio element not found");
    }
  }

function noElec(){ // Message qui s'affiche quand on n'a pas d'électricité
    let title = "Pas d'élec";
    document.getElementById("TitreModal").innerHTML = title.toString();
    let message = "Tu n'as pas d'électricité";
    document.getElementById("modalBody").innerHTML = message.toString();
    isModalOpen = false;
}

////////////////////////////////////////////////////////////////////////////////

// CODE POUR LE PC

////////////////////////////////////////////////////////////////////////////

function hidePcPopup() {
    const pcPopup = document.getElementById('pcpopupContainer');
    pcPopup.style.display = 'none';
}

function seConnecter() {
    const casValue = document.getElementById("cas").value;
    const passwordValue = document.getElementById("password").value;

    if (casValue === "sbouchardon" && passwordValue === "psgldc2024") {
        window.location.href = "../../HTML/sim_pc/bureau.html";
    } else {
        alert("Nom d'utilisateur ou mot de passe incorrect. Veuillez réessayer.");
    }
}

function seDeconnecter() {
    window.location.href = "../../HTML/sim_pc/login.html";
}

function ouvrirConsole() {
    const consoleElement = document.getElementById("console");
    consoleElement.style.display = "block";
}

function fermerConsole() {
    const consoleElement = document.getElementById("console");
    consoleElement.style.display = "none";
}

// ... (previous functions)

// Function to reset the console to its default state
function resetConsole() {
    const consoleContent = document.querySelector('.console');
    consoleContent.innerHTML = '<div class="close-button" onclick="fermerConsole()">✖</div>';
    consoleContent.innerHTML += '<p class="prompt">Invite de commande: \'MAN\' pour afficher les commandes</p>';
    consoleContent.innerHTML += '<p class="prompt">C:/Users/Bureau></p>';
    consoleContent.innerHTML += '<input class="prompt" type="text" id="commandInput" onkeydown="executerCommande(event)"></div>';
}

let droitsCorbeille = "333"; // Remplacez par la valeur réelle des droits
let repertoire = "Bureau";

function executerCommande(event) {
    let inputElement;
    inputElement = document.getElementById('commandInput');
    const command = inputElement.value;
    const consoleContent = document.querySelector('.console');
    if (event.key === 'Enter') {

        if (command.toUpperCase() === "MAN") {
            consoleContent.innerHTML = '<div class="close-button" onclick="fermerConsole()">✖</div>';
            consoleContent.innerHTML += '<p class="prompt">MAN commande - Afficher les commandes disponibles:</p>';
            consoleContent.innerHTML += '<p class="prompt">Pour simplement ouvrir un fichier, entrez son nom.</p>';
            consoleContent.innerHTML += '<p class="prompt">    Exemple: pour ouvrir "mon_fichier.txt", écrire "mon_fichier.txt"</p>';
            consoleContent.innerHTML += '<p class="prompt">             puis appuyer sur Entrée</p>';
            consoleContent.innerHTML += '<p class="prompt">chmod - Modifie les bits de mode du fichier.</p>';
            consoleContent.innerHTML += '<p class="prompt">   Le programme chmod est utilisé pour modifier les permissions d\'accès</p>';
            consoleContent.innerHTML += '<p class="prompt">   sur un fichier ou un répertoire dans un système Unix/Linux.</p>';
            consoleContent.innerHTML += '<p class="prompt">   Les permissions définissent qui peut lire, écrire et exécuter un fichier.</p>';
            consoleContent.innerHTML += '<p class="prompt">   Exemple d\'utilisation :</p>';
            consoleContent.innerHTML += '<p class="prompt">      chmod fichier.txt 755</p>';
            consoleContent.innerHTML += '<p class="prompt">      (attention aux GI qui vont se trigger)</p>';
            consoleContent.innerHTML += '<p class="prompt">   Cela donne au propriétaire toutes les permissions (lecture, écriture et exécution),</p>';
            consoleContent.innerHTML += '<p class="prompt">   et aux autres (groupe et monde) la permission de lecture et d\'exécution.</p>';
            consoleContent.innerHTML += '<p class="prompt">ls - Affiche des informations sur les fichiers dans le répertoire actuel.</p>';
            consoleContent.innerHTML += '<p class="prompt">cd - Change le répertoire de travail actuel.</p>';
            consoleContent.innerHTML += '<p class="prompt">   Exemple: cd Corbeille</p>';
            consoleContent.innerHTML += '<p class="prompt">   -> Change le répertoire actuel vers la Corbeille</p>';
            consoleContent.innerHTML += '<p class="prompt">mv - Déplace un fichier vers un autre répertoire.</p>';
            consoleContent.innerHTML += '<p class="prompt">   Exemple: mv mon_fichier.txt ../mon_dossier/</p>';
            consoleContent.innerHTML += '<p class="prompt">   -> Déplace mon_fichier.txt vers le répertoire ../mon_dossier/</p>';
            consoleContent.innerHTML += '<p class="prompt">cp - Copie un fichier et le colle dans un répertoire</p>';
            consoleContent.innerHTML += '<p class="prompt">   Exemple: cp mon_fichier.txt ../mon_dossier/</p>';
            consoleContent.innerHTML += '<p class="prompt">   -> Copie mon_fichier.txt et le colle dans le répertoire ../mon_dossier/</p>';
            consoleContent.innerHTML += '<p class="prompt">rm * - Te donne une issue de secours (ou pas ;-) )</p>';
            consoleContent.innerHTML += '<p class="prompt">Appuyer sur \'q\' pour quitter MAN</p>';
            // Listen for 'q' key press to reset the console
            document.addEventListener('keydown', function(event) {
                if (event.key === 'q') {
                    resetConsole();
                }
            });

        } else if (command.toUpperCase() === "LS") {
            // Mettez à jour la commande LS pour afficher les fichiers appropriés en fonction du répertoire
            if (repertoire === "Bureau/Corbeille") {
                consoleContent.innerHTML = '<div class="close-button" onclick="fermerConsole()">✖</div>';
                consoleContent.innerHTML += '<p class="prompt">Liste des fichiers et répertoires:</p>';
                consoleContent.innerHTML += '<p class="prompt">- droits_badgeuse.txt</p>';
                consoleContent.innerHTML += '<p class="prompt">- DO_NOT_OPEN.exe</p>';
                consoleContent.innerHTML += '<p class="prompt">C:/Users/' + repertoire + '></p>';
                consoleContent.innerHTML += '<input class="prompt" type="text" id="commandInput" onkeydown="executerCommande(event)"></div>';
            }
            else {
                // Mettez à jour avec les fichiers du répertoire par défaut (Bureau)
                consoleContent.innerHTML = '<div class="close-button" onclick="fermerConsole()">✖</div>';
                consoleContent.innerHTML += '<p class="prompt">Liste des fichiers et répertoires:</p>';
                consoleContent.innerHTML += '<p class="prompt">- Badgeuse</p>';
                consoleContent.innerHTML += '<p class="prompt">- Corbeille</p>';
                consoleContent.innerHTML += '<p class="prompt">- Documents</p>';
                consoleContent.innerHTML += '<p class="prompt">C:/Users/' + repertoire + '></p>';
                consoleContent.innerHTML += '<input class="prompt" type="text" id="commandInput" onkeydown="executerCommande(event)"></div>';
            }
        }

        else if (command.toUpperCase() === "DROITS_BADGEUSE.TXT") {
            if (repertoire === "Bureau/Corbeille") {
                consoleContent.innerHTML = '<div class="close-button" onclick="fermerConsole()">✖</div>';
                consoleContent.innerHTML += '<p class="prompt">// Fichier: droits_badgeuse.txt</p>';
                consoleContent.innerHTML += '<p class="output">=====================================================</p>';
                consoleContent.innerHTML += '<p class="output">AVIS D\'ACCÈS DE SÉCURITÉ - BADGEUSE</p>';
                consoleContent.innerHTML += '<p class="output">=====================================================</p>';
                consoleContent.innerHTML += '<p class="output">Ce document atteste que l\'utilisateur a les droits d\'accès nécessaires à la badgeuse de la salle FA403.</p>';
                consoleContent.innerHTML += '<p class="output">Nom de l\'utilisateur: A*£¨%42/°_</p>';
                consoleContent.innerHTML += '<p class="output">Identifiant d\'accès: £%FQ53FS£°</p>';
                consoleContent.innerHTML += '<p class="output">Niveau d\'autorisation: 7</p>';
                consoleContent.innerHTML += '<p class="output">Droits d\'accès accordés:</p>';
                consoleContent.innerHTML += '<p class="output">- Ouverture et fermeture de la badgeuse</p>';
                consoleContent.innerHTML += '<p class="output">- Vérification des autorisations d\'accès</p>';
                consoleContent.innerHTML += '<p class="output">- Enregistrement des transactions d\'accès</p>';
                consoleContent.innerHTML += '<p class="output">=====================================================</p>';
                consoleContent.innerHTML += '<p class="output">Code de redémarrage de la badgeuse: 0727</p>';
                consoleContent.innerHTML += '<p class="output">Code de réinitialisation de la badgeuse: 0543</p>';
                consoleContent.innerHTML += '<p class="output">=====================================================</p>';
                consoleContent.innerHTML += '<p class="output">ATTENTION: CE DOCUMENT EST STRICTEMENT FICTIF</p>';
                consoleContent.innerHTML += '<p class="output">=====================================================</p>';
                consoleContent.innerHTML += '<p class="prompt">C:/Users/' + repertoire + '></p>';
                consoleContent.innerHTML += '<input class="prompt" type="text" id="commandInput" onkeydown="executerCommande(event)"></div>';
            }
            else {
                // Mettez à jour avec les fichiers du répertoire par défaut (Bureau)
                consoleContent.innerHTML = '<div class="close-button" onclick="fermerConsole()">✖</div>';
                consoleContent.innerHTML += '<p class="prompt">Erreur: fichier introuvable.</p>';
                consoleContent.innerHTML += '<p class="prompt">C:/Users/' + repertoire + '></p>';
                consoleContent.innerHTML += '<input class="prompt" type="text" id="commandInput" onkeydown="executerCommande(event)"></div>';
            }
        }

        else if (command.toUpperCase() === "MV DROITS_BADGEUSE.TXT ../BADGEUSE/" || command.toUpperCase() === "CP DROITS_BADGEUSE.TXT ../BADGEUSE/") {
            if (repertoire === "Bureau/Corbeille") {
                consoleContent.innerHTML = '<div class="close-button" onclick="fermerConsole()">✖</div>';
                consoleContent.innerHTML += '<p class="prompt">Droits d\'accès restaurés avec succès.</p>';
                consoleContent.innerHTML += '<p class="prompt">C:/Users/' + repertoire + '></p>';
                consoleContent.innerHTML += '<input class="prompt" type="text" id="commandInput" onkeydown="executerCommande(event)"></div>';
            }
        }

        else if (command.toUpperCase().startsWith("CHMOD CORBEILLE")) {
            // Extraire les nouveaux droits de la commande
            const nouveauxDroits = command.split(' ')[2];

            // Vérifier si les nouveaux droits sont valides (vous pouvez ajouter une logique plus avancée ici)
            if ((nouveauxDroits[0] >= 4 && nouveauxDroits[0] <= 7) && nouveauxDroits[1]>=0 && nouveauxDroits[1] <= 7 && nouveauxDroits[2] <= 7 && nouveauxDroits[2] >= 0 && nouveauxDroits.length === 3) {
                // Mettre à jour les droits de la Corbeille
                droitsCorbeille = nouveauxDroits;

                // Afficher un message de confirmation dans la console
                consoleContent.innerHTML = '<div class="close-button" onclick="fermerConsole()">✖</div>';
                consoleContent.innerHTML += '<p class="prompt">Droits de la Corbeille mis à jour avec succès.</p>';
                consoleContent.innerHTML += '<p class="prompt">C:/Users/' + repertoire + '></p>';
                consoleContent.innerHTML += '<input class="prompt" type="text" id="commandInput" onkeydown="executerCommande(event)"></div>';
            }
            else {
                // Afficher un message d'erreur si les nouveaux droits ne sont pas valides
                consoleContent.innerHTML = '<div class="close-button" onclick="fermerConsole()">✖</div>';
                consoleContent.innerHTML += '<p class="prompt">Erreur : Nouveaux droits invalides.</p>';
                consoleContent.innerHTML += '<p class="prompt">C:/Users/' + repertoire + '></p>';
                consoleContent.innerHTML += '<input class="prompt" type="text" id="commandInput" onkeydown="executerCommande(event)"></div>';
            }
        }
        else if (command.toUpperCase() === "CD CORBEILLE") {
            // Vérifier si l'utilisateur a le droit d'accéder à la Corbeille
            if (droitsCorbeille.charAt(0) !== '5' && droitsCorbeille.charAt(0) !== '7') {
                consoleContent.innerHTML = '<div class="close-button" onclick="fermerConsole()">✖</div>';
                consoleContent.innerHTML += '<p class="prompt">Erreur : Vous n\'avez pas les droits pour accéder à la Corbeille.</p>';
                consoleContent.innerHTML += '<p class="prompt">C:/Users/' + repertoire + '></p>';
                consoleContent.innerHTML += '<input class="prompt" type="text" id="commandInput" onkeydown="executerCommande(event)"></div>';
            }
            else {

                // Afficher un message de confirmation dans la console
                consoleContent.innerHTML = '<div class="close-button" onclick="fermerConsole()">✖</div>';
                consoleContent.innerHTML += '<p class="prompt">Répertoire actuel : Corbeille</p>';
                consoleContent.innerHTML += '<p class="prompt">C:/Users/' + repertoire + '/Corbeille></p>';
                consoleContent.innerHTML += '<input class="prompt" type="text" id="commandInput" onkeydown="executerCommande(event)"></div>';
                // Changer le répertoire courant vers la Corbeille
                repertoire = "Bureau/Corbeille";
            }
        }
        else if (command.toUpperCase() === "CD ..") {
            // Empêcher CD .. lorsque dans le répertoire Bureau
            if (repertoire === "Bureau") {
                consoleContent.innerHTML = '<div class="close-button" onclick="fermerConsole()">✖</div>';
                consoleContent.innerHTML += '<p class="prompt">Erreur : Vous n\'avez pas les droits.</p>';
                consoleContent.innerHTML += '<p class="prompt">C:/Users/' + repertoire + '></p>';
                consoleContent.innerHTML += '<input class="prompt" type="text" id="commandInput" onkeydown="executerCommande(event)"></div>';
            }
            else if (repertoire === "Bureau/Corbeille") {
                // Revenir au répertoire Bureau
                repertoire = "Bureau";
                consoleContent.innerHTML = '<div class="close-button" onclick="fermerConsole()">✖</div>';
                consoleContent.innerHTML += '<p class="prompt">Répertoire actuel : Bureau</p>';
                consoleContent.innerHTML += '<p class="prompt">C:/Users/Bureau></p>';
                consoleContent.innerHTML += '<input class="prompt" type="text" id="commandInput" onkeydown="executerCommande(event)"></div>';
            }
        }
        else if (command.toUpperCase() === "RM *") {
            window.location.href = "blue_screen.html";

        }
        else {
            consoleContent.innerHTML = '<div class="close-button" onclick="fermerConsole()">✖</div>';
            consoleContent.innerHTML += '<p class="prompt">Commande inconnue</p>';
            consoleContent.innerHTML += '<p class="prompt">\'MAN\' pour afficher les commandes</p>';
            consoleContent.innerHTML += '<p class="prompt">C:/Users/' + repertoire + '></p>';
            consoleContent.innerHTML += '<input class="prompt" type="text" id="commandInput" onkeydown="executerCommande(event)"></div>';
        }

        // Effacer le champ d'entrée
        inputElement.value = '';
        inputElement = document.getElementById('commandInput');
        inputElement.focus();
    }
    else if (event.key === 'Tab') {
        event.preventDefault(); // Évite que la touche Tab déclenche un changement de focus par défaut
        inputElement.focus();
    }
}

const percentageElement = document.getElementById("percentage");
let percentage = 0;

function process() {
    percentage += parseInt(Math.random() * 10);
    if (percentage > 100) {
        percentage = 100;
    }
    percentageElement.innerText = percentage;
    processInterval();
}

function processInterval() {
    setTimeout(process, Math.random() * (1000 - 500) + 500)
}
processInterval();

//////////////////////////////////////////////////////////////////////////////////////////////////

// FIN DU CODE POUR PC

//////////////////////////////////////////////////////////////////////////////////////////////
