let tutorialStarted = false;

function playAudio() {
  var backgroundAudio = document.getElementById('fire');
  backgroundAudio.play();
  backgroundAudio.volume = 0.4;
}

function playClickSound() {
  var clickSound = document.getElementById("clickSoundTrouve");
  if (clickSound) {
    clickSound.volume = 0.2;
    clickSound.play();
  } else {
    console.error("Audio element not found");
  }
}
function startGame() {
  let gameContainer = document.getElementById('game-container');
  let tutoContainer = document.getElementById('tuto-container');

  if (!tutorialStarted) {
    // Show tutorial container and hide game container
    gameContainer.style.display = 'none';
    tutoContainer.style.display = 'block';

    // Set the flag to true
    tutorialStarted = true;

    // Wait for 3 seconds and then allow redirection
    setTimeout(() => {
      enableRedirection();
    }, 3000);
  } else {
    // Redirect to another page
    redirectToAnotherPage();
  }
}

function enableRedirection() {
  // Now, when the button is clicked again, it will redirect to another page
  document.getElementById('start-button').textContent = 'Redirect to Another Page';
}

function redirectToAnotherPage() {
  // Replace 'your_destination_url' with the actual URL you want to redirect to
  window.location.href = 'HTML/index.html';
}

  
  function pop (e) {
    let amount = 30;
    switch (e.target.dataset.type) {
      case 'shadow':
      case 'line':
        amount = 60;
        break;
    }
    // Quick check if user clicked the button using a keyboard
    if (e.clientX === 0 && e.clientY === 0) {
      const bbox = e.target.getBoundingClientRect();
      const x = bbox.left + bbox.width / 2;
      const y = bbox.top + bbox.height / 2;
      for (let i = 0; i < 30; i++) {
        // We call the function createParticle 30 times
        // We pass the coordinates of the button for x & y values
        createParticle(x, y, e.target.dataset.type);
      }
    } else {
      for (let i = 0; i < amount; i++) {
        createParticle(e.clientX, e.clientY + window.scrollY, e.target.dataset.type);
      }
    }
  }
  function createParticle (x, y) {
    const particle = document.createElement('particle');
    document.body.appendChild(particle);
    let width;
    let height;
    let destinationX = (Math.random() - 0.5) * 300;
    let destinationY = (Math.random() - 0.5) * 300;
    let rotation = Math.random() * 520;
    let delay = Math.random() * 200;

    const color = `hsl(${Math.random() * 60}, 100%, 60%)`;
    particle.style.boxShadow = `0 0 ${Math.floor(Math.random() * 10 + 10)}px ${color}`;
    particle.style.background = color;
    particle.style.borderRadius = '50%';
    width = height = Math.random() * 5 + 4;
    
    
    particle.style.width = `${width}px`;
    particle.style.height = `${height}px`;
    const animation = particle.animate([
      {
        transform: `translate(-50%, -50%) translate(${x}px, ${y}px) rotate(0deg)`,
        opacity: 1
      },
      {
        transform: `translate(-50%, -50%) translate(${x + destinationX}px, ${y + destinationY}px) rotate(${rotation}deg)`,
        opacity: 0
      }
    ], {
      duration: Math.random() * 1000 + 5000,
      easing: 'cubic-bezier(0, .9, .57, 1)',
      delay: delay
    });
    animation.onfinish = removeParticle;
  }
  function removeParticle (e) {
    e.srcElement.effect.target.remove();
  }
  
  if (document.body.animate) {
    document.querySelectorAll('button').forEach(button => button.addEventListener('click', pop));
  }
  