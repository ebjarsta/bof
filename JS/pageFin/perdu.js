document.addEventListener("DOMContentLoaded", function () {
    // You can add JavaScript code here if needed
    // This file is included for potential future use
  
    // Add an event listener to the button
    const button = document.querySelector("#overlay button");
    button.addEventListener("click", function () {
      // Redirect the user to the specified page
      window.location.href = "../../index.html";
    });
  });
  
 