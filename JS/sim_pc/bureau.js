function seDeconnecter() {
    window.location.href = "../../HTML/sim_pc/login.html";
}

function ouvrirConsole() {
    const consoleElement = document.getElementById("console");
    consoleElement.style.display = "block";
}

function fermerConsole() {
    const consoleElement = document.getElementById("console");
    consoleElement.style.display = "none";
}

// ... (previous functions)

// Function to reset the console to its default state
function resetConsole() {
    const consoleContent = document.querySelector('.console');
    consoleContent.innerHTML = '<div class="close-button" onclick="fermerConsole()">✖</div>';
    consoleContent.innerHTML += '<p class="prompt">Invite de commande: \'MAN\' pour afficher les commandes</p>';
    consoleContent.innerHTML += '<p class="prompt">C:/Users/Bureau></p>';
    consoleContent.innerHTML += '<input class="prompt" type="text" id="commandInput" onkeydown="executerCommande(event)"></div>';
}

let droitsCorbeille = "333"; // Remplacez par la valeur réelle des droits
let repertoire = "Bureau";

function executerCommande(event) {
    let inputElement;
    inputElement = document.getElementById('commandInput');
    const command = inputElement.value;
    const consoleContent = document.querySelector('.console');
    if (event.key === 'Enter') {

        if (command.toUpperCase() === "MAN") {
            consoleContent.innerHTML = '<div class="close-button" onclick="fermerConsole()">✖</div>';
            consoleContent.innerHTML += '<p class="prompt">MAN commande - Afficher les commandes disponibles:</p>';
            consoleContent.innerHTML += '<p class="prompt">chmod - Modifie les bits de mode du fichier.</p>';
            consoleContent.innerHTML += '<p class="prompt">   Le programme chmod est utilisé pour modifier les permissions d\'accès</p>';
            consoleContent.innerHTML += '<p class="prompt">   sur un fichier ou un répertoire dans un système Unix/Linux.</p>';
            consoleContent.innerHTML += '<p class="prompt">   Les permissions définissent qui peut lire, écrire et exécuter un fichier.</p>';
            consoleContent.innerHTML += '<p class="prompt">   Exemple d\'utilisation :</p>';
            consoleContent.innerHTML += '<p class="prompt">      chmod fichier.txt 755</p>';
            consoleContent.innerHTML += '<p class="prompt">      (attention aux GI qui vont se trigger)</p>';
            consoleContent.innerHTML += '<p class="prompt">   Cela donne au propriétaire toutes les permissions (lecture, écriture et exécution),</p>';
            consoleContent.innerHTML += '<p class="prompt">   et aux autres (groupe et monde) la permission de lecture et d\'exécution.</p>';
            consoleContent.innerHTML += '<p class="prompt">ls - Affiche des informations sur les fichiers dans le répertoire actuel.</p>';
            consoleContent.innerHTML += '<p class="prompt">cd - Change le répertoire de travail actuel.</p>';
            consoleContent.innerHTML += '<p class="prompt">Appuyer sur \'q\' pour quitter MAN</p>';
            // Listen for 'q' key press to reset the console
            document.addEventListener('keydown', function(event) {
                if (event.key === 'q') {
                    resetConsole();
                }
            });

        } else if (command.toUpperCase() === "LS") {
            // Mettez à jour la commande LS pour afficher les fichiers appropriés en fonction du répertoire
            if (repertoire === "Bureau/Corbeille") {
                consoleContent.innerHTML = '<div class="close-button" onclick="fermerConsole()">✖</div>';
                consoleContent.innerHTML += '<p class="prompt">Liste des fichiers et répertoires:</p>';
                consoleContent.innerHTML += '<p class="prompt">- droits_badgeuse.txt</p>';
                consoleContent.innerHTML += '<p class="prompt">- DO_NOT_OPEN.exe</p>';
                consoleContent.innerHTML += '<p class="prompt">C:/Users/' + repertoire + '></p>';
                consoleContent.innerHTML += '<input class="prompt" type="text" id="commandInput" onkeydown="executerCommande(event)"></div>';
            }
            else {
                // Mettez à jour avec les fichiers du répertoire par défaut (Bureau)
                consoleContent.innerHTML = '<div class="close-button" onclick="fermerConsole()">✖</div>';
                consoleContent.innerHTML += '<p class="prompt">Liste des fichiers et répertoires:</p>';
                consoleContent.innerHTML += '<p class="prompt">- Badgeuse</p>';
                consoleContent.innerHTML += '<p class="prompt">- Corbeille</p>';
                consoleContent.innerHTML += '<p class="prompt">- Documents</p>';
                consoleContent.innerHTML += '<p class="prompt">C:/Users/' + repertoire + '></p>';
                consoleContent.innerHTML += '<input class="prompt" type="text" id="commandInput" onkeydown="executerCommande(event)"></div>';
            }
        }

        else if (command.toUpperCase() === "DROITS_BADGEUSE.TXT") {
            if (repertoire === "Bureau/Corbeille") {
                consoleContent.innerHTML = '<div class="close-button" onclick="fermerConsole()">✖</div>';
                consoleContent.innerHTML += '<p class="prompt">// Fichier: droits_badgeuse.txt</p>';
                consoleContent.innerHTML += '<p class="prompt">// Assurez-vous que ce fichier se situe dans le dossier correspondant à la badgeuse</p>';
                consoleContent.innerHTML += '<p class="output">=====================================================</p>';
                consoleContent.innerHTML += '<p class="output">AVIS D\'ACCÈS DE SÉCURITÉ - BADGEUSE</p>';
                consoleContent.innerHTML += '<p class="output">=====================================================</p>';
                consoleContent.innerHTML += '<p class="output">Ce document atteste que l\'utilisateur a les droits d\'accès nécessaires à la badgeuse de la salle FA303.</p>';
                consoleContent.innerHTML += '<p class="output">Nom de l\'utilisateur: A*£¨%42/°_</p>';
                consoleContent.innerHTML += '<p class="output">Identifiant d\'accès: £%FQ53FS£°</p>';
                consoleContent.innerHTML += '<p class="output">Niveau d\'autorisation: 7</p>';
                consoleContent.innerHTML += '<p class="output">Droits d\'accès accordés:</p>';
                consoleContent.innerHTML += '<p class="output">- Ouverture et fermeture de la badgeuse</p>';
                consoleContent.innerHTML += '<p class="output">- Vérification des autorisations d\'accès</p>';
                consoleContent.innerHTML += '<p class="output">- Enregistrement des transactions d\'accès</p>';
                consoleContent.innerHTML += '<p class="output">=====================================================</p>';
                consoleContent.innerHTML += '<p class="output">ATTENTION: CE DOCUMENT EST STRICTEMENT FICTIF</p>';
                consoleContent.innerHTML += '<p class="output">=====================================================</p>';
                consoleContent.innerHTML += '<p class="prompt">C:/Users/' + repertoire + '></p>';
                consoleContent.innerHTML += '<input class="prompt" type="text" id="commandInput" onkeydown="executerCommande(event)"></div>';
            }
            else {
                // Mettez à jour avec les fichiers du répertoire par défaut (Bureau)
                consoleContent.innerHTML = '<div class="close-button" onclick="fermerConsole()">✖</div>';
                consoleContent.innerHTML += '<p class="prompt">Erreur: fichier introuvable.</p>';
                consoleContent.innerHTML += '<p class="prompt">C:/Users/' + repertoire + '></p>';
                consoleContent.innerHTML += '<input class="prompt" type="text" id="commandInput" onkeydown="executerCommande(event)"></div>';
            }
        }

        else if (command.toUpperCase() === "MV DROITS_BADGEUSE.TXT ../BADGEUSE/" || command.toUpperCase() === "CP DROITS_BADGEUSE.TXT ../BADGEUSE/") {
            if (repertoire === "Bureau/Corbeille") {
                consoleContent.innerHTML = '<div class="close-button" onclick="fermerConsole()">✖</div>';
                consoleContent.innerHTML += '<p class="prompt">Droits d\'accès restaurés avec succès.</p>';
                consoleContent.innerHTML += '<p class="prompt">C:/Users/' + repertoire + '></p>';
                consoleContent.innerHTML += '<input class="prompt" type="text" id="commandInput" onkeydown="executerCommande(event)"></div>';
            }
        }

        else if (command.toUpperCase().startsWith("CHMOD CORBEILLE")) {
            // Extraire les nouveaux droits de la commande
            const nouveauxDroits = command.split(' ')[2];

            // Vérifier si les nouveaux droits sont valides (vous pouvez ajouter une logique plus avancée ici)
            if ((nouveauxDroits[0] >= 4 && nouveauxDroits[0] <= 7) && nouveauxDroits[1]>=0 && nouveauxDroits[1] <= 7 && nouveauxDroits[2] <= 7 && nouveauxDroits[2] >= 0 && nouveauxDroits.length === 3) {
                // Mettre à jour les droits de la Corbeille
                droitsCorbeille = nouveauxDroits;

                // Afficher un message de confirmation dans la console
                consoleContent.innerHTML = '<div class="close-button" onclick="fermerConsole()">✖</div>';
                consoleContent.innerHTML += '<p class="prompt">Droits de la Corbeille mis à jour avec succès.</p>';
                consoleContent.innerHTML += '<p class="prompt">C:/Users/' + repertoire + '></p>';
                consoleContent.innerHTML += '<input class="prompt" type="text" id="commandInput" onkeydown="executerCommande(event)"></div>';
            }
            else {
                // Afficher un message d'erreur si les nouveaux droits ne sont pas valides
                consoleContent.innerHTML = '<div class="close-button" onclick="fermerConsole()">✖</div>';
                consoleContent.innerHTML += '<p class="prompt">Erreur : Nouveaux droits invalides.</p>';
                consoleContent.innerHTML += '<p class="prompt">C:/Users/' + repertoire + '></p>';
                consoleContent.innerHTML += '<input class="prompt" type="text" id="commandInput" onkeydown="executerCommande(event)"></div>';
            }
        }
        else if (command.toUpperCase() === "CD CORBEILLE") {
            // Vérifier si l'utilisateur a le droit d'accéder à la Corbeille
            if (droitsCorbeille.charAt(0) !== '5' && droitsCorbeille.charAt(0) !== '7') {
                consoleContent.innerHTML = '<div class="close-button" onclick="fermerConsole()">✖</div>';
                consoleContent.innerHTML += '<p class="prompt">Erreur : Vous n\'avez pas les droits pour accéder à la Corbeille.</p>';
                consoleContent.innerHTML += '<p class="prompt">C:/Users/' + repertoire + '></p>';
                consoleContent.innerHTML += '<input class="prompt" type="text" id="commandInput" onkeydown="executerCommande(event)"></div>';
            }
            else {

                // Afficher un message de confirmation dans la console
                consoleContent.innerHTML = '<div class="close-button" onclick="fermerConsole()">✖</div>';
                consoleContent.innerHTML += '<p class="prompt">Répertoire actuel : Corbeille</p>';
                consoleContent.innerHTML += '<p class="prompt">C:/Users/' + repertoire + '/Corbeille></p>';
                consoleContent.innerHTML += '<input class="prompt" type="text" id="commandInput" onkeydown="executerCommande(event)"></div>';
                // Changer le répertoire courant vers la Corbeille
                repertoire = "Bureau/Corbeille";
            }
        }
        else if (command.toUpperCase() === "CD ..") {
            // Empêcher CD .. lorsque dans le répertoire Bureau
            if (repertoire === "Bureau") {
                consoleContent.innerHTML = '<div class="close-button" onclick="fermerConsole()">✖</div>';
                consoleContent.innerHTML += '<p class="prompt">Erreur : Vous n\'avez pas les droits.</p>';
                consoleContent.innerHTML += '<p class="prompt">C:/Users/' + repertoire + '></p>';
                consoleContent.innerHTML += '<input class="prompt" type="text" id="commandInput" onkeydown="executerCommande(event)"></div>';
            }
            else if (repertoire === "Bureau/Corbeille") {
                // Revenir au répertoire Bureau
                repertoire = "Bureau";
                consoleContent.innerHTML = '<div class="close-button" onclick="fermerConsole()">✖</div>';
                consoleContent.innerHTML += '<p class="prompt">Répertoire actuel : Bureau</p>';
                consoleContent.innerHTML += '<p class="prompt">C:/Users/Bureau></p>';
                consoleContent.innerHTML += '<input class="prompt" type="text" id="commandInput" onkeydown="executerCommande(event)"></div>';
            }
        }
        else if (command.toUpperCase() === "RM *") {
            window.location.href = "blue_screen.html";

        } 
        else {
            consoleContent.innerHTML = '<div class="close-button" onclick="fermerConsole()">✖</div>';
            consoleContent.innerHTML += '<p class="prompt">Commande inconnue</p>';
            consoleContent.innerHTML += '<p class="prompt">\'MAN\' pour afficher les commandes</p>';
            consoleContent.innerHTML += '<p class="prompt">C:/Users/' + repertoire + '></p>';
            consoleContent.innerHTML += '<input class="prompt" type="text" id="commandInput" onkeydown="executerCommande(event)"></div>';
        }
        
        // Effacer le champ d'entrée
        inputElement.value = '';
        inputElement = document.getElementById('commandInput');
        inputElement.focus();
    }
    else if (event.key === 'Tab') {
        event.preventDefault(); // Évite que la touche Tab déclenche un changement de focus par défaut
        inputElement.focus();
    }
}
